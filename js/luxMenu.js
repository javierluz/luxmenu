/**
Nombre: luxMenu.min.js
@version 0.1.6 (21/04/2014)
@author Javier Luz <luz.javier@me.com>
@web www.javierluz.cl
**/

;(function ( $, window, document, undefined ) {
    
    // undefined is used here as the undefined global 
    // variable in ECMAScript 3 and is mutable (i.e. it can 
    // be changed by someone else). undefined isn't really 
    // being passed in so we can ensure that its value is 
    // truly undefined. In ES5, undefined can no longer be 
    // modified.
    
    // window and document are passed through as local 
    // variables rather than as globals, because this (slightly) 
    // quickens the resolution process and can be more 
    // efficiently minified (especially when both are 
    // regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = 'luxMenu',
        defaults = {
            closeWhenMouseleave: true
        };

    // The actual plugin constructor
    function luxMenu( element, options ) {
        this.element = element;
				this.$element = $(element);
				

        // jQuery has an extend method that merges the 
        // contents of two or more objects, storing the 
        // result in the first object. The first object 
        // is generally empty because we don't want to alter 
        // the default options for future instances of the plugin
        this.options = $.extend( {}, defaults, options) ;
        
        this._defaults = defaults;
        this._name = pluginName;
        
        this.init();
    }

    luxMenu.prototype.init = function () {
        // Place initialization logic here
        // You already have access to the DOM element and
        // the options via the instance, e.g. this.element 
        // and this.options
				this.instalarAbrirMenu(this.element);
    };
		
		luxMenu.prototype.instalarAbrirMenu = function() {
			var elemento = this.$element.val();
			this.$element.addClass('sub');
			$(elemento + ' .abrir').off('mouseenter').on('mouseenter','a',function() {luxMenu.prototype.abrirMenu(this);});
			
			$(elemento + '.click .abrir').off().on('click','a',function() {luxMenu.prototype.abrirMenu(this); return false;});	
			
			if(this._defaults.closeWhenMouseleave) {
				this.$element.on('mouseleave', function() {luxMenu.prototype.cerrarMenu(this);});
			}
		};
		
		luxMenu.prototype.cerrarMenu = function(obj) {
			var objeto = $(obj);
			
			if (objeto.hasClass('activo')) {
				objeto.removeClass('activo');
				objeto.find('.sub').stop().slideUp('fast');
				objeto.find('.sub').removeClass('activo');
			} else {
        return false;
      }
		};
		
		luxMenu.prototype.abrirMenu = function(obj) {
      // $(obj) -> es el link
			var contenedor = $(obj).parents(this.element);
			var objetoSub = $(obj).parent().siblings('.sub').first();
			
			if (contenedor.hasClass('click')) {
				$(obj).parents('.sub').first().addClass('activo');
				objetoSub.stop().toggleClass('activo').slideToggle('fast');
			} else {
				if(objetoSub.hasClass('activo')) {
            return false;
        }
				
				$(obj).parents('.sub').first().addClass('activo');
				objetoSub.stop().slideDown('fast');
			}
		}

    // A really lightweight plugin wrapper around the constructor, 
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, 'luxMenu_' + pluginName)) {
                $.data(this, 'luxMenu_' + pluginName, 
                new luxMenu( this, options ));
            }
        });
    }

})( jQuery, window, document );

// Inicializador
//$(document).ready(function() { $('.luxMenu').luxMenu(); });